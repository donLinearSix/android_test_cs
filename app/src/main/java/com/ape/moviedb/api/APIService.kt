package com.ape.moviedb.api

import com.ape.moviedb.model.NowPlayingMovie
import com.ape.moviedb.model.NowPlayingResult
import com.ape.moviedb.model.PopularMovie
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIService {

    @GET("3/movie/now_playing")
    suspend fun getNowShowingMoviesFromAPI(
        @Query("language") language: String,
        @Query("page") page: String,
        @Query("api_key") apiKey: String
    ): Response<NowPlayingMovie>

    @GET("3/movie/popular")
    suspend fun getPopularMoviesFromAPI(
        @Query("language") language: String,
        @Query("page") page: Int,
        @Query("api_key") apiKey: String
    ): Response<PopularMovie>


//
}