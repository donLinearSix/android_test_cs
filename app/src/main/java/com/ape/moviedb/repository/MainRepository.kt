package com.ape.moviedb.repository

import com.ape.moviedb.BuildConfig
import com.ape.moviedb.api.APIService

class MainRepository(private val apiService: APIService) {

    suspend fun getNowShowingMovies(page: String) =
        apiService.getNowShowingMoviesFromAPI(BuildConfig.LANGUAGE, page, BuildConfig.API_KEY)

    suspend fun getPopularMovies(page: Int) =
        apiService.getPopularMoviesFromAPI(BuildConfig.LANGUAGE, page, BuildConfig.API_KEY)
}