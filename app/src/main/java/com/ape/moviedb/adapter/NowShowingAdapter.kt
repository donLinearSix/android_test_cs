package com.ape.moviedb.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ape.moviedb.R
import com.ape.moviedb.databinding.NowShowingItemBinding
import com.ape.moviedb.model.NowPlayingResult
import com.ape.moviedb.utils.Utils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class NowShowingAdapter(
    private val results: ArrayList<NowPlayingResult>,
    private val context: Context
) : RecyclerView.Adapter<NoShowingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoShowingViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: NowShowingItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.now_showing_item, parent, false)
        return NoShowingViewHolder(binding, context)
    }

    override fun getItemCount(): Int {
        return results.size
    }

    override fun onBindViewHolder(holder: NoShowingViewHolder, position: Int) {
        holder.bind(results.get(position))
    }

    fun setList(resultsList: List<NowPlayingResult>) {
        results.addAll(resultsList)

    }

}

class NoShowingViewHolder(private val binding: NowShowingItemBinding, private val context: Context) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(movie: NowPlayingResult) {

        val imgUrl = "https://image.tmdb.org/t/p/original/" + movie.posterPath;


        Glide.with(context)
            .load(imgUrl)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(binding.poster)

    }
}