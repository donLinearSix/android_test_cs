package com.ape.moviedb.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ape.moviedb.R
import com.ape.moviedb.databinding.MovieItemBinding
import com.ape.moviedb.model.NowPlayingResult
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlin.collections.ArrayList

class MoviesAdapter(
    private val results: ArrayList<NowPlayingResult>,
    private val context: Context
) :
    RecyclerView.Adapter<MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: MovieItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.movie_item, parent, false)
        return MovieViewHolder(binding,context)
    }

    override fun getItemCount(): Int {
        return results.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(results[position])
    }

    fun setList(resultsList: List<NowPlayingResult>) {
        results.addAll(resultsList)

    }

}

class MovieViewHolder(private val binding: MovieItemBinding, private val context: Context) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(movie: NowPlayingResult) {
        binding.title.text = movie.title
        binding.releaseDate.text = movie.releaseDate

        val imgUrl = "https://image.tmdb.org/t/p/original/" + movie.posterPath;


        Glide.with(context)
            .load(imgUrl)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(binding.poster)
    }
}