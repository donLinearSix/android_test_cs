package com.ape.moviedb.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.ape.moviedb.model.NowPlayingResult
import com.ape.moviedb.repository.MainRepository
import com.ape.moviedb.utils.Utils
import kotlinx.coroutines.Dispatchers
import java.util.*
import androidx.lifecycle.liveData
import com.ape.moviedb.utils.Resource

class MainViewModel(private val mainRepository: MainRepository) : ViewModel() {


        val nowShowingLiveData = liveData  {
            val response = mainRepository.getNowShowingMovies("1")//undefined
            emit(response)
        }

        val popularLiveData = liveData  {
            val response = mainRepository.getPopularMovies(1)
            emit(response)
        }


    fun getNowShowing() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getNowShowingMovies("1")))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }



}