package com.ape.moviedb

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ape.moviedb.adapter.MoviesAdapter
import com.ape.moviedb.adapter.NowShowingAdapter
import com.ape.moviedb.api.RetrofitInstance
import com.ape.moviedb.databinding.ActivityMainBinding
import com.ape.moviedb.model.NowPlayingResult
import com.ape.moviedb.repository.MainRepository
import com.ape.moviedb.utils.Status
import com.ape.moviedb.utils.Utils
import com.ape.moviedb.vm.MainViewModel
import com.ape.moviedb.vmfactory.HomeViewModelFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MoviesAdapter
    private lateinit var nowShowingAdapter: NowShowingAdapter
    private var popularMovies = ArrayList<NowPlayingResult>()
    private var nowShowingMovies = ArrayList<NowPlayingResult>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val repository = MainRepository(RetrofitInstance.apiService)
        val factory = HomeViewModelFactory(repository)
        viewModel = ViewModelProvider(this, factory).get(MainViewModel::class.java)
        binding.bindViewModel = viewModel
        binding.lifecycleOwner = this

        initRecyclerView()


    }


    private fun initRecyclerView() {
        binding.popularRecyclerView!!.layoutManager = LinearLayoutManager(this)
        adapter = MoviesAdapter(popularMovies, this)
        binding.popularRecyclerView!!.adapter = adapter

        displayPopularMovies()


        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding.nowShowingRecyclerView!!.layoutManager = linearLayoutManager
        binding.nowShowingRecyclerView!!.isNestedScrollingEnabled = true
        binding.nowShowingRecyclerView!!.setHasFixedSize(true)

        nowShowingAdapter = NowShowingAdapter(nowShowingMovies, this)
        binding.nowShowingRecyclerView!!.adapter = nowShowingAdapter

        displayNowShowingMovies()

    }

    /**
     * get popular API list
     */
    private fun displayPopularMovies() {
        viewModel.popularLiveData.observe(this, Observer {
            adapter.setList(it.body()!!.results)
            adapter.notifyDataSetChanged()
        })
    }

    /**
     * get now showing API list with coroutines
     */
    private fun displayNowShowingMovies() {
        viewModel.getNowShowing().observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { list -> updateNowShowingList(list.body()!!.results) }
                    }
                    Status.ERROR -> {
                        Toast.makeText(this, "ERROR ON API LOADING", Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        /**
                         * Can use progressbar until data is loading.
                         */
                    }
                }
            }
        })
    }

    /**
     * update list
     */
    private fun updateNowShowingList(users: List<NowPlayingResult>) {
        adapter.apply {
            nowShowingAdapter.setList(users)
            nowShowingAdapter.notifyDataSetChanged()
        }
    }
}
