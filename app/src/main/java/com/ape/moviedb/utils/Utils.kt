package com.ape.moviedb.utils

import android.util.Log
import com.ape.moviedb.BuildConfig

class Utils {

    companion object {

        /**
         * print log data
         */
        fun log(tag: String?, debugMessage: String?) {
            if (BuildConfig.DEBUG) {
                Log.e(tag, debugMessage)
            }
        }
    }
}