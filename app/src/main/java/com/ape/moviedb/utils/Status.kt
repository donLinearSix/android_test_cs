package com.ape.moviedb.utils

enum class Status {

    SUCCESS,
    ERROR,
    LOADING
}