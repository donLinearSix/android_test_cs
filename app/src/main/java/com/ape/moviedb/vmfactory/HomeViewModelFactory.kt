package com.ape.moviedb.vmfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ape.moviedb.api.APIService
import com.ape.moviedb.repository.MainRepository
import com.ape.moviedb.vm.MainViewModel

class HomeViewModelFactory (private val repository: MainRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}