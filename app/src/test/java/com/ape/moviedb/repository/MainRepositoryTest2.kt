package com.ape.moviedb.repository

import androidx.lifecycle.LiveData
import com.ape.moviedb.model.NowPlayingResult
import com.ape.moviedb.model.PopularMovie
import com.ape.moviedb.vm.MainViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import java.util.*
import kotlin.collections.ArrayList


class MainRepositoryTest2 {

    @Mock
    var repository: MainRepository? = null

    private lateinit var viewModel: MainViewModel

    @Before
    fun setUp() {
    }

    @Test
   suspend fun getNowShowingMovies() {

       var allCategories =  repository!!.getNowShowingMovies("1")
        Assert.assertFalse(allCategories.isSuccessful)
    }
}